<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @inheritdoc
 *
 * @property Order $order
 *
 */
class Order extends ActiveRecord {
	const STATUS_SUBMITTED  = 'SUBMITTED';
	const STATUS_COMPLETED  = 'COMPLETED';
	const SCENARIO_INSERT   = 'insert';
	const SCENARIO_UPDATE   = 'update';

	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return ArrayHelper::merge(parent::rules(), [
			[['customer_email', 'sub_total_amt', 'tax_amt', 'total_amt', 'status'], 'required', 'skipOnError' => true,
			],
			[['customer_email'], 'email', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['sub_total_amt'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['tax_amt'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['total_amt'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['status'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]				
			],
			[['status'], 'in', 'skipOnError' => true,
				'range' => [self::STATUS_SUBMITTED, self::STATUS_COMPLETED],
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['country'], 'string', 'skipOnError' => true,
				'length' => 2,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['format'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]				
			],
			[['format'], 'in', 'skipOnError' => true,
				'range' => ['JSON', 'HTML', 'PDF'],
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['send_mode'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]				
			],
			[['send_mode'], 'in', 'skipOnError' => true,
				'range' => ['EMAIL', 'NULL'],
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return ArrayHelper::merge(parent::attributeLabels(), [
			'customer_email'     => Yii::t('app', 'Customer Email'),
			'sub_total_amt' => Yii::t('app', 'Sub Total Amount'),
			'tax_amt' => Yii::t('app', 'Tax Amount'),
			'total_amt' => Yii::t('app', 'Total Amount'),
			'status' => Yii::t('app', 'Status'),
		]);
	}
	public function findById($id) {
		$sql = Order::find()
			->andWhere(['id' => $id])
			->one();

		return $sql;
	}
}
