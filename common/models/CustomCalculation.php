<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%custom_calculation}}".
 *
 * @inheritdoc
 *
 * @property CustomCalculation $calculation
 *
 */
class CustomCalculation extends ActiveRecord {
	const OPERATION_ADD       = 'ADD';
	const OPERATION_SUBSTRACT = 'SUBSTRACT';
	const OPERATION_DIVIDE  = 'DIVIDE';
	const OPERATION_MULTIPLY  = 'MULTIPLY';
	const SCENARIO_INSERT = 'insert';
	const SCENARIO_UPDATE = 'update';

	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return ArrayHelper::merge(parent::rules(), [
			[['max_price', 'operation', 'amount'], 'required', 'skipOnError' => true,
			],
			[['max_price'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['amount'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['operation'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]				
			],
			[['operation'], 'in', 'skipOnError' => true,
				'range' => [self::OPERATION_ADD, self::OPERATION_SUBSTRACT, self::OPERATION_DIVIDE, self::OPERATION_MULTIPLY],
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return ArrayHelper::merge(parent::attributeLabels(), [

		]);
	}
}
