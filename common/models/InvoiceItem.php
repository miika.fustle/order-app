<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_item}}".
 *
 * @inheritdoc
 *
 * @property Order $order
 *
 */
class InvoiceItem extends ActiveRecord {
	const SCENARIO_INSERT = 'insert';
	const SCENARIO_UPDATE = 'update';
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return ArrayHelper::merge(parent::rules(), [
			[['name', 'qty','line_amt', 'line_tax_amt', 'line_tax', 'line_total_amt'], 'required', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['name'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['line_amt'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['qty'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['line_tax_amt'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['line_tax'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['line_total_amt'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return ArrayHelper::merge(parent::attributeLabels(), [
			'name'     => Yii::t('app', 'Product Name'),
			'line_amt' => Yii::t('app', 'Sub Total Amount'),
			'line_tax_amt' => Yii::t('app', 'Tax Amount'),
			'line_tax' => Yii::t('app', 'Tax Percent'),
			'line_total_amt' => Yii::t('app', 'Total Amount'),
		]);
	}
}
