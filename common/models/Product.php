<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product}}".
 *
 * @inheritdoc
 *
 * @property Order $order
 *
 */
class Product extends ActiveRecord {
	const SCENARIO_INSERT = 'insert';
	const SCENARIO_UPDATE = 'update';
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return ArrayHelper::merge(parent::rules(), [
			[['name', 'category_id', 'price'], 'required', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['name'], 'string', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['category_id'], 'integer', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
			[['price'], 'number', 'skipOnError' => true,
				'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]
			],
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return ArrayHelper::merge(parent::attributeLabels(), [
			'name'     => Yii::t('app', 'Product Name'),
			'category_id' => Yii::t('app', 'Product Category'),
			'price' => Yii::t('app', 'Price'),
		]);
	}

	public function getPriceByNameByCountry($name,$country) {
		$sql = Product::find()
			->select(['price'])
			->andWhere(['name' => $name])
			->andWhere(['country' => $country])
			->one();

		return $sql;
	}
	public function getTaxByNameByCountry($name,$country) {
		$sql = Product::find()
			->select(['tax_percent'])
			->andWhere(['name' => $name])
			->andWhere(['country' => $country])
			->one();

		return $sql;	
	}
	
}
