<?php
namespace frontend\controllers;
use Yii;
use yii\rest\ActiveController;

use common\models\Product;

/**
 * Order controller
 */
class ProductController extends ActiveController {

	public $modelClass = 'common\models\Product';

	public function behaviors() {
		return [
			[
				'class' => \yii\filters\ContentNegotiator::className(),
				'only' => ['index'],
				'formats' => [
					'application/json' => \yii\web\Response::FORMAT_JSON,
				],
			],
		];
	}

	public function actions() {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}
