<?php
namespace frontend\controllers\calculation;

use Yii;
use yii\rest\UpdateAction AS BaseUpdateAction;

use common\models\CustomCalculation;

class UpdateAction extends BaseUpdateAction {
	public $scenario = CustomCalculation::SCENARIO_UPDATE;

	public function run($id) {
        $calculation = $this->findModel($id);
		$calculation->setScenario(CustomCalculation::SCENARIO_UPDATE);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $calculation);
        }
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$post = json_decode(Yii::$app->request->getRawBody(), true);

			if(empty($post)){
				$post = Yii::$app->request->post();
			}
			$txn = Yii::$app->db->beginTransaction();
			
			try {
				$calculation->max_price = $post['max_price'];
				$calculation->operation = $post['operation'];
				$calculation->amount =  $post['amount'];
				$result = $calculation->save();
				if($result) {
					
					$txn->commit();
					return array('status' => true, 'message' => 'Calculation has been updated');
				}

			}catch(\Exception $e) {
				$txn->rollback();
				return array('status' => false,'error_message' => $e->getMessage());
			}



	}
}
