<?php
namespace frontend\controllers\calculation;

use Yii;
use yii\rest\CreateAction AS BaseCreateAction;
use common\models\Order;
use common\models\OrderItem;
use common\models\CustomCalculation;

class CreateAction extends BaseCreateAction {
	public $scenario = CustomCalculation::SCENARIO_INSERT;

	public function run() {
		//$calculationExists = CustomCalculation::find()->where(['id' => 1])->exists();
		if($calculationExists=false) {
			return array('status' => false, 'message' => 'Calculation exist please update it');
		} else {
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$post = Yii::$app->request->post();
	
			if(empty($post)){
				$post = json_decode(Yii::$app->request->getRawBody(), true);
			}
			$txn = Yii::$app->db->beginTransaction();
			try {
				$calculation = new CustomCalculation(['scenario' => CustomCalculation::SCENARIO_INSERT]);
				$calculation->max_price = $post['max_price'];
				$calculation->operation = $post['operation'];
				$calculation->amount = $post['amount'];
				$result = $calculation->save();
	
				$txn->commit();
			} catch(\Exception $e) {
				$txn->rollback();
				return array('status' => false, 'error_message' => $e );
			}
			if($result) {
				return array('status' => true, 'message' => 'Custom Calculation has been saved');		
			} else {	
				return array('status' => false,'error_messages' => $order->getErrors());
			}
		}

	}
}
