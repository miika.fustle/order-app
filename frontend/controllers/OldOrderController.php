<?php
namespace frontend\controllers;
use Yii;
use yii\rest\Controller;
use yii\web\Response;
use common\models\Order;
use common\models\OrderItem;
use common\models\Product;
use common\models\Invoice;
use common\models\InvoiceItem;
/**
 * Site controller
 */
class OldOrderController extends Controller {
	public $modelClass = 'common\models\Order';
	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_HTML;
		return $behaviors;
	}
   /* public function actions() {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'frontend\controllers\order\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }*/

	public function actionListOrders() {
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$orders = Order::find()->all();
		
		if(count($orders) > 0) {
			return array('status' => true, 'data' => $orders);
		} else {
			return array('status'=>false,'data'=> Yii::t('app' , 'No Orders Found'));		
		}
		
	}
	public function actionCreateOrder() {
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$post = Yii::$app->request->post();

		if(empty($post)){
			$post = json_decode(Yii::$app->request->getRawBody(), true);
		}
		$total_amt = 0;
		$sub_total_amt = 0;
		$tax_amt = 0;
		$txn = Yii::$app->db->beginTransaction();
		try {
			$order = new Order(['scenario' => Order::SCENARIO_INSERT]);
			$order->customer_email = $post['customer_email'];
			$order->format = $post['format'];
			$order->country = $post['country'];
			if(empty($post['send_mode'])) {
				$order->send_mode = null;
			} else {
				$order->send_mode = $post['send_mode'];
			}
			$order->status = Order::STATUS_SUBMITTED;
	
			$arrOrderItems = $post['products'];
			$orderItems = [];
			foreach($arrOrderItems AS $orderItem) {
				if($orderItem['qty'] > 0) {
					$newOrderItem = new OrderItem(['scenario' => OrderItem::SCENARIO_INSERT]);
					$newOrderItem->name = $orderItem['name'];
					$newOrderItem->qty = $orderItem['qty'];
					$unitPrice = Product::getPriceByNameByCountry($newOrderItem->name,$order->country);
					$taxPercent = Product::getTaxByNameByCountry($newOrderItem->name,$order->country);
					$newOrderItem->line_amt = $unitPrice->price * $newOrderItem->qty;
					$newOrderItem->line_tax = $taxPercent->tax_percent;
					$newOrderItem->line_tax_amt = $newOrderItem->line_amt * $newOrderItem->line_tax;	
					$newOrderItem->line_total_amt = $newOrderItem->line_amt + $newOrderItem->line_tax_amt;
					$orderItems[] = $newOrderItem;
					$total_amt += $newOrderItem->line_total_amt;
					$sub_total_amt += $newOrderItem->line_amt;
					$tax_amt += $newOrderItem->line_tax_amt;
				}
			}
			$order->total_amt = $total_amt;
			$order->sub_total_amt = $sub_total_amt;
			$order->tax_amt = $tax_amt;
			$result = $order->save();
			foreach($orderItems AS $orderItem) {
				$orderItem->order_id = $order->id;
				$orderItem->save();
			}
			$txn->commit();
		} catch(\Exception $e) {
			$txn->rollback();
			return array('status' => false, 'error_message' => $e );
		}
		if($result) {
			return array('status' => true, 'message' => 'Order has been Sent');		
		} else {	
			return array('status' => false,'error_messages' => $order->getErrors());
		}

	}
	public function actionGetProducts() {
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_HTML;
		$products = Product::find()->all();
		
		if(count($products) > 0) {
			$dataProvider = new ArrayDataProvider([
				'allModels' => Authors::find()->all(),
			]);
			return $this->render('index',[
			'dataProvider' => $dataProvider,
		]);	
		} else {
			return array('status'=>false,'data'=> Yii::t('app' , 'No Products Found'));		
		}

	}
	public function actionEdit() {
		\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$post = json_decode(Yii::$app->request->getRawBody(), true);
		$id = $post['order_id'];
		//$additional_price = $post['add_price'];
		if(!isset($additional_price)) {
			$additional_price = 0;
		}

		$order = Order::findById($id);
		if($order->status == Order::STATUS_SUBMITTED) {
			$orderItems = OrderItem::findByOrderId($order->id);
			$txn = Yii::$app->db->beginTransaction();
			try{
				$invoice = new Invoice(['scenario' => Invoice::SCENARIO_INSERT]);
				$invoice->order_id = $order->id;
				$invoice->customer_email = $order->customer_email;
				$invoice->send_mode = $order->send_mode;
				$invoice->format = $order->format;
				$invoice->country = $order->country;
				$invoice->sub_total_amt = $order->sub_total_amt + $additional_price;
				$invoice->tax_amt = $order->tax_amt;
				$invoice->total_amt = $order->total_amt + $additional_price;
				$invoice->status = Order::STATUS_COMPLETED;
				$invoice->save();
				$invoiceItems = [];
				foreach($orderItems AS $orderItem) {
					$invoiceItem = new InvoiceItem(['scenario' => InvoiceItem::SCENARIO_INSERT]);
					$invoiceItem->invoice_id = $invoice->id;
					$invoiceItem->order_item_id = $orderItem->id;
					$invoiceItem->name = $orderItem->name;
					$invoiceItem->qty = ''.$orderItem->qty;
					$invoiceItem->line_amt = $orderItem->line_amt;
					$invoiceItem->line_tax = $orderItem->line_tax;
					$invoiceItem->line_tax_amt = $orderItem->line_tax_amt;
					$invoiceItem->line_total_amt = $orderItem->line_total_amt;
					$invoiceItem->save();
					$invoiceItems[] = $invoiceItem;
				}
				$order->setScenario(Order::SCENARIO_UPDATE);
				$order->status = Order::STATUS_COMPLETED;
				$order->save();
				$txn->commit();
				if($invoice->send_mode=='EMAIL') {
					$mailer = Yii::$app->mailer;
					if($invoice->format == 'JSON') {
						/*$mailer
							->compose([
								'text' => 'invoice-text',
							], [
								'invoice' => $invoice,
							])
							->setFrom('reiska.piupau@gmail.com')
							->setTo($invoice->customer_email)
							->setSubject(Yii::t('app', 'Invoice {invoice_id}', ['invoice_id' => $invoice->id]))
							->send();*/
						return array('status' => true, 'invoice' => $invoice,'invoice_items' => $invoiceItems, 'sent_to_email' => true);
					} else if($invoice->format == 'HTML') {
						\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
						/*$mailer
							->compose([
								'text' => 'invoice-html',
							], [
								'invoice' => $invoice,
							])
							->setFrom('reiska.piupau@gmail.com')
							->setTo($invoice->customer_email)
							->setSubject(Yii::t('app', 'Invoice {invoice_id}', ['invoice_id' => $invoice->id]))
							->send();*/

							return $this->render('invoice', [
								'model' => $invoice,
							]);

					} else {
						return array('status' => true, 'message' => yii::t('app','Functionality to other formats are on progress'));
					}
				} else {
					if($invoice->format == 'JSON') {
						return array('status' => true, 'invoice' => $invoice, 'invoice_items' => $invoiceItems);
					} else if($invoice->format == 'HTML') {
						\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
						//it goes here
						$view = $this->renderPartial('invoice', [
							'invoice' => $invoice,
						]);
						return $view;
					} else {
						yii::trace('tännne');
						return array('status' => true, 'message' => yii::t('app','Functionality to other formats are on progress'));
					}
				}
			}catch(\Exception $e) {
				$txn->rollback();
				return array('status' => false,'error_message' => $e);
			}

		} else {
			return array('status' => false, 'error_message' => Yii::t('app', 'Order is invalid'));
		}
	}
}
