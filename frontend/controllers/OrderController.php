<?php
namespace frontend\controllers;
use Yii;
use yii\rest\ActiveController;

use common\models\Order;

/**
 * Order controller
 */
class OrderController extends ActiveController {

	public $modelClass = 'common\models\Order';

    public $createScenario = Order::SCENARIO_INSERT;
    public $updateScenario = Order::SCENARIO_UPDATE;
	
	public function behaviors() {
		return [
			[
				'class' => \yii\filters\ContentNegotiator::className(),
				'only' => ['index'],
				'formats' => [
					'application/json' => \yii\web\Response::FORMAT_JSON,
				],
			],
		];
	}
	
	public function actions() {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'frontend\controllers\order\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'frontend\controllers\order\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

}
