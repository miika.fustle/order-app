<?php
namespace frontend\controllers;
use Yii;
use yii\rest\ActiveController;

use common\models\CustomCalculation;

/**
 * Order controller
 */
class CalculationController extends ActiveController {

	public $modelClass = 'common\models\CustomCalculation';



	public function actions() {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'frontend\controllers\calculation\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'frontend\controllers\calculation\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
        ];
    }

}
