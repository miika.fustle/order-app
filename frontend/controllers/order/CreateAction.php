<?php
namespace frontend\controllers\order;

use Yii;
use yii\rest\CreateAction AS BaseCreateAction;
use common\models\Order;
use common\models\OrderItem;
use common\models\Product;
use common\models\CustomCalculation;

class CreateAction extends BaseCreateAction {
	public $scenario = Order::SCENARIO_INSERT;

	public function run() {
\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
		$post = Yii::$app->request->post();
		if(empty($post)){
			$post = json_decode(Yii::$app->request->getRawBody(), true);
		}
		$total_amt = 0;
		$sub_total_amt = 0;
		$tax_amt = 0;
		$txn = Yii::$app->db->beginTransaction();
		try {
			$order = new Order(['scenario' => Order::SCENARIO_INSERT]);
			$calculation = CustomCalculation::find()->where(['id' => 1])->one();
			$order->customer_email = $post['customer_email'];
			$order->format = $post['format'];
			$order->country = $post['country'];
			if(empty($post['send_mode'])) {
				$order->send_mode = null;
			} else {
				$order->send_mode = $post['send_mode'];
			}
			$order->status = Order::STATUS_SUBMITTED;
	
			$arrOrderItems = $post['products'];
			$orderItems = [];
			foreach($arrOrderItems AS $orderItem) {
				if($orderItem['qty'] > 0) {
					$newOrderItem = new OrderItem(['scenario' => OrderItem::SCENARIO_INSERT]);
					$newOrderItem->name = $orderItem['name'];
					$newOrderItem->qty = $orderItem['qty'];
					$unitPrice = Product::getPriceByNameByCountry($newOrderItem->name,$order->country);
					$taxPercent = Product::getTaxByNameByCountry($newOrderItem->name,$order->country);
					$newOrderItem->line_amt = $unitPrice->price * $newOrderItem->qty;
					$newOrderItem->line_tax = $taxPercent->tax_percent;
					$newOrderItem->line_tax_amt = $newOrderItem->line_amt * $newOrderItem->line_tax;	
					$newOrderItem->line_total_amt = $newOrderItem->line_amt + $newOrderItem->line_tax_amt;
					$orderItems[] = $newOrderItem;
					$total_amt += $newOrderItem->line_total_amt;
					$sub_total_amt += $newOrderItem->line_amt;
					$tax_amt += $newOrderItem->line_tax_amt;
				}
			}
			if(isset($calculation)) {
				if($calculation->max_price > $total_amt) {
					switch($calculation->operation) {
						case('ADD'):
							$order->total_amt = $total_amt + $calculation->amount;
							$order->sub_total_amt = $sub_total_amt;
							$order->tax_amt = $tax_amt;
						break;
						case('SUBSTRACT'):
							$order->total_amt = $total_amt - $calculation->amount;
							$order->sub_total_amt = $sub_total_amt;
							$order->tax_amt = $tax_amt;
						break;
						case('DIVIDE'):
							$order->total_amt = $total_amt / $calculation->amount;
							$order->sub_total_amt = $sub_total_amt;
							$order->tax_amt = $tax_amt;
						break;
						case('MULTIPLY'):
							$order->total_amt = $total_amt * $calculation->amount;
							$order->sub_total_amt = $sub_total_amt;
							$order->tax_amt = $tax_amt;
						break;
					}
				} else {
					$order->total_amt = $total_amt;
					$order->sub_total_amt = $sub_total_amt;
					$order->tax_amt = $tax_amt;			
				}
			} else {
				$order->total_amt = $total_amt;
				$order->sub_total_amt = $sub_total_amt;
				$order->tax_amt = $tax_amt;
			}
			$result = $order->save();
			foreach($orderItems AS $orderItem) {
				$orderItem->order_id = $order->id;
				$orderItem->save();
			}
			$txn->commit();
		} catch(\Exception $e) {
			$txn->rollback();
			return array('status' => false, 'message' => $e );
		}
		if($result) {
			return array('status' => true, 'message' => 'Order has been Sent');		
		} else {	
			return array('status' => false,'message' => $order->getErrors());
		}

}
}
