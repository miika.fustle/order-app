<?php
namespace frontend\controllers\order;

use Yii;
use yii\helpers\Json;
use yii\rest\UpdateAction AS BaseUpdateAction;
use common\models\Order;
use common\models\OrderItem;
use common\models\Product;
use common\models\Invoice;
use common\models\InvoiceItem;

class UpdateAction extends BaseUpdateAction {
	public $scenario = Order::SCENARIO_UPDATE;

	public function run($id) {
        $order = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $order);
        }
\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;



		$post = json_decode(Yii::$app->request->getRawBody(), true);

		if($order->status == Order::STATUS_SUBMITTED) {
			$orderItems = OrderItem::findByOrderId($order->id);
			$txn = Yii::$app->db->beginTransaction();

			try {

				$invoice = new Invoice(['scenario' => Invoice::SCENARIO_INSERT]);
				$invoice->order_id = $order->id;
				$invoice->customer_email = $order->customer_email;
				$invoice->send_mode = $order->send_mode;
				$invoice->format = $order->format;
				$invoice->country = $order->country;
			if(isset($post['max_price'])) {
				if($post['max_price'] > $order->total_amt) {
					switch($post['operation']) {
						case('ADD'):
							$invoice->sub_total_amt = $order->sub_total_amt;
							$invoice->tax_amt = $order->tax_amt;
							$invoice->total_amt = $order->total_amt + $post['amount'];
						break;
						case('SUBSTRACT'):
							$invoice->sub_total_amt = $order->sub_total_amt;
							$invoice->tax_amt = $order->tax_amt;
							$invoice->total_amt = $order->total_amt - $post['amount'];
						break;
						case('DIVIDE'):
							$invoice->sub_total_amt = $order->sub_total_amt;
							$invoice->tax_amt = $order->tax_amt;
							$invoice->total_amt = $order->total_amt / $post['amount'];
						break;
						case('MULTIPLY'):
							$invoice->sub_total_amt = $order->sub_total_amt;
							$invoice->tax_amt = $order->tax_amt;
							$invoice->total_amt = $order->total_amt * $post['amount'];
						break;
					}
				} else {
					$invoice->sub_total_amt = $order->sub_total_amt;
					$invoice->tax_amt = $order->tax_amt;
					$invoice->total_amt = $order->total_amt;		
				}
			} else {
				$invoice->sub_total_amt = $order->sub_total_amt;
				$invoice->tax_amt = $order->tax_amt;
				$invoice->total_amt = $order->total_amt;				
			}
				$invoice->status = Order::STATUS_COMPLETED;
				$invoice->save();

				$invoiceItems = [];
				foreach($orderItems AS $orderItem) {
					$invoiceItem = new InvoiceItem(['scenario' => InvoiceItem::SCENARIO_INSERT]);
					$invoiceItem->invoice_id = $invoice->id;
					$invoiceItem->order_item_id = $orderItem->id;
					$invoiceItem->name = $orderItem->name;
					$invoiceItem->qty = ''.$orderItem->qty;
					$invoiceItem->line_amt = $orderItem->line_amt;
					$invoiceItem->line_tax = $orderItem->line_tax;
					$invoiceItem->line_tax_amt = $orderItem->line_tax_amt;
					$invoiceItem->line_total_amt = $orderItem->line_total_amt;
					$invoiceItem->save();
					$invoiceItems[] = $invoiceItem;
				}

				$order->setScenario(Order::SCENARIO_UPDATE);
				$order->status = Order::STATUS_COMPLETED;
				$order->save();

				$txn->commit();

				$invoice = Invoice::findByOrderId($id);
				if($invoice->send_mode=='EMAIL') {
					$mailer = Yii::$app->mailer;
					if($invoice->format == 'JSON') {
					
						$mailer
							->compose([
								'text' => 'invoice-text',
							], [
								'invoice' => Json::encode($invoice),
								'invoiceItems' => Json::encode($invoiceItems),
							])
							->setFrom('miika@fustle.io')
							->setTo($invoice->customer_email)
							->setSubject(Yii::t('app', 'Invoice {invoice_id}', ['invoice_id' => $invoice->id]))
							->send();
					
						return array('status' => true, 'invoice' => $invoice,'invoice_items' => $invoiceItems, 'sent_to_email' => true);
					} else if($invoice->format == 'HTML') {
						 Yii::$app->response->headers->set('Content-type', 'text/html; charset=UTF-8');
					
						$mailer
							->compose([
								'html' => 'invoice-html',
							], [
								'invoice' => $invoice,
								'invoiceItems' => $invoiceItems,
							])
							->setFrom('miika@fustle.io')
							->setTo($invoice->customer_email)
							->setSubject(Yii::t('app', 'Invoice {invoice_id}', ['invoice_id' => $invoice->id]))
							->send();
					
							// TODO: set response type



						$view = $this->controller->render('invoice', [
							'invoice' => $invoice,

						]);
						return $view;
					} else {
						return array('status' => true, 'message' => yii::t('app', 'Functionality to other formats are on progress'));
					}
				} else {
					if($invoice->format == 'JSON') {
						return array('status' => true, 'invoice' => $invoice, 'invoice_items' => $invoiceItems);
					} else if($invoice->format == 'HTML') {
						Yii::$app->response->headers->set('Content-type', 'text/html; charset=UTF-8');
						//it goes here

						$view = $this->controller->render('invoice', [
							'invoice' => $invoice,

						]);
						return $view;
					} else {
						yii::trace('tännne');
						return array('status' => true, 'message' => yii::t('app','Functionality to other formats are on progress'));
					}
				}
			}catch(\Exception $e) {
				$txn->rollback();
				return array('status' => false,'message' => $e->getMessage());
			}

		} else {
			return array('status' => false, 'message' => Yii::t('app', 'Invalid Order status {status}', ['status' => $order->status]));
		}

	}
}
