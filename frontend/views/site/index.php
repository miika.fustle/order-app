<?php
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
$this->title = 'Order Application';

?>
<div class="site-index">
<h1><?= yii::t('app','in this application you are able to create the order') ?> </h1>

</div>
<?php

$this->registerJs(
"
$.ajax({
	type: 'GET',
	url: 'http://localhost/order-app/frontend/web/order/get-products',
	success: function(result){
	$.each(result['data'], function(i, item) {
		console.log(result['data'][i].name)
    $('<tr>').html(
        '<td>' + result['data'][i].name + '</td><td>' + result['data'][i].category + '</td><td>' + result['data'][i].price + '</td>').appendTo('#product_table');
});
	}
});
", \yii\web\View::POS_END);
?>