<?php
use yii\grid\GridView;

$this->title = 'Create Order';

?>
<div class="order-view">
<h1>Order Form</h1>
<form id="order-form">
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Email:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="email" type="email"/>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Invoice Send Mode:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		 <select id="send_mode">
			<option value=""></option>
			<option value="EMAIL">email</option>
		</select> 
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Country:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="country">
			<option value="FI">Finland</option>
			<option value="SE">Sweden</option>
		</select> 
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Invoice Format:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="format">
			<option value="JSON">json</option>
			<option value="HTML">html</option>
			<option value="PDF">pdf</option>
		</select>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Item Name:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="item_name6">
			<option value="MILK">Milk</option>

		</select> 
	</div>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Quantity:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="qty6" type="number" value="0"/>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Item Name:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="item_name1">

			<option value="CHEESE">Cheese</option>

		</select> 
	</div>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Quantity:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="qty1" type="number" value="0"/>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Item Name:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="item_name2">
			<option value="SHOES">Shoes</option>
		</select> 
	</div>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Quantity:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="qty2" type="number" value="0"/>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Item Name:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="item_name3">
			<option value="JACKET">Jacket</option>
		</select> 
	</div>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Quantity:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="qty3" type="number" value="0"/>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Item Name:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="item_name4">
			<option value="TV">Tv</option>
		</select> 
	</div>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Quantity:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="qty4" type="number" value="0"/>
	</div>
</div>
<div>&nbsp;</div>
<div class="row">
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Item Name:
	</div>

	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<select id="item_name5">
			<option value="PHONE">Phone</option>
		</select> 
	</div>
	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
		Quantity:
	</div>
	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
		<input id="qty5" type="number" value="0"/>
	</div>
</div>

 
	
</form>
<button id="create_order">Send Order</button>


</div>
<?php

$this->registerJs(
"
$('#create_order').click(function(){
	var orderItemsArray = [{'name':$('#item_name1').val(),'qty':$('#qty1').val()},{'name':$('#item_name2').val(),'qty':$('#qty2').val()},{'name':$('#item_name3').val(),'qty':$('#qty3').val()},{'name':$('#item_name4').val(),'qty':$('#qty4').val()},{'name':$('#item_name5').val(),'qty':$('#qty5').val()},{'name':$('#item_name6').val(),'qty':$('#qty6').val()}];
	var orderObject = {'customer_email':$('#email').val(),'format':$('#format').val(),'country':$('#country').val(),'send_mode':$('#send_mode').val(),'products':orderItemsArray};
$.ajax({
	type: 'POST',
	url: 'http://localhost/order-app/frontend/web/order/create-order',
	data : orderObject,
	success: function(result){
		if(result['status'] = true) {
			alert(result['message']);
		} else {
			alert(result['error_message']);
		}

	}
});
});

", \yii\web\View::POS_END);
?>