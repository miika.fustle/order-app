<?php

use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\detail\DetailView;
use common\models\Invoice;

$this->title = Yii::t('app', 'Invoice');

$invoiceItemDataProvider = new ArrayDataProvider([
	'allModels' => $invoice->invoiceItems,
	'pagination' => false,
	'sort' => false,
]);

?>
<div class="invoice-view">
	<div class="invoice-customer">
	<?= DetailView::widget([
		'model'          => $invoice,
		'mode'           => DetailView::MODE_VIEW,
		'enableEditMode' => false,
		'bordered'       => false,
		'striped'        => true ,
		'condensed'      => true ,
		'responsive'     => true ,
	//	'hover'          => false,
		'panel'          => [
			'type'    => DetailView::TYPE_DEFAULT,
			'heading' => $this->title,
		],
		'attributes' => [
			'attribute' => 'id',
			'attribute' => 'order_id',
			'attribute' => 'customer_email',
			'attribute' => 'sub_total_amt',
			'attribute' => 'tax_amt',
			'attribute' => 'total_amt',
		],
	]); ?>
	</div>
</div>
