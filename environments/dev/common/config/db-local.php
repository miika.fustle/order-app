<?php

$db_host = 'localhost';
$db_port = '3306';
$db_name = 'order_db';
$db_user = 'Shuuya_tech';
$db_pass = 'Shuuya_tech';

return [
	'class' => 'yii\db\Connection',
	'dsn' => 'mysql:host=' . $db_host . ';port=' . $db_port . ';dbname=' . $db_name,
	'username' => $db_user,
	'password' => $db_pass,
	'charset' => 'utf8',
	'tablePrefix' => 't_',
	//'enableSchemaCache' => true,
	//'schemaCacheDuration' => 1800,
];
?>
