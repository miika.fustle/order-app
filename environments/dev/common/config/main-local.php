<?php
return [
	'components' => [
		'db' => require(__DIR__ . '/../../common/config/db-local.php'),
		'mailer' => require(__DIR__ . '/../../common/config/mailer-local.php'),
	],
];
