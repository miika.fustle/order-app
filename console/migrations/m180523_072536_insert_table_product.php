<?php

use yii\db\Migration;

/**
 * Class m180523_072536_insert_table_product
 */
class m180523_072536_insert_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$sql =
		"INSERT INTO t_product (name ,category,country,price,tax_percent, created_by,updated_by) VALUES
			('MILK'  ,'GROCERY',    'FI'   ,1   ,0.1400 ,1, 1),
			('CHEESE','GROCERY',    'FI'   ,4   ,0.1400 ,1, 1),
			('SHOES' ,'CLOTHES',    'FI'   ,20  ,0.0500 ,1, 1),
			('JACKET','CLOTHES',    'FI'   ,99  ,0.0500 ,1, 1),
			('TV'    ,'ELECTRONICS','FI'   ,500 ,0.1300 ,1, 1),
			('PHONE' ,'ELECTRONICS','FI'   ,700 ,0.1300 ,1, 1),
			('MILK'  ,'GROCERY',    'SE'   ,1   ,0.0700 ,1, 1),
			('CHEESE','GROCERY',    'SE'   ,4   ,0.0700 ,1, 1),
			('SHOES' ,'CLOTHES',    'SE'   ,20  ,0.0250 ,1, 1),
			('JACKET','CLOTHES',    'SE'   ,99  ,0.0250 ,1, 1),
			('TV'    ,'ELECTRONICS','SE'   ,500 ,0.0650 ,1, 1),
			('PHONE' ,'ELECTRONICS','SE'   ,700 ,0.0650 ,1, 1)
		";
		$this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$sql =
		"DELETE FROM t_product WHERE name IN (
			'MILK'                ,
			'CHEESE'              ,
			'SHOES'               ,
			'JACKET'              ,
			'TV'                  ,
			'PHONE'        		  
		)";
		$this->execute($sql);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_072536_insert_table_product cannot be reverted.\n";

        return false;
    }
    */
}
