<?php

use yii\db\Migration;

/**
 * Class m180523_135422_create_table_invoice_item
 */
class m180523_135422_create_table_invoice_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%invoice_item}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'invoice_id'              => $this -> integer(10)    -> unsigned() -> notNull()                                           ,
			'order_item_id'           => $this -> integer(10)    -> unsigned() -> notNull()                                           ,
			'name'                    => $this -> text()                       -> notNull()                                           ,
			'qty'                     => $this -> integer(10)                  -> notNull()                                           ,
			'line_amt'    			  => $this -> decimal(10,2)                -> notNull()                                           ,
			'line_tax'                => $this -> decimal(10,2)                -> notNull()                                           ,
			'line_tax_amt'            => $this -> decimal(10,2)                -> notNull()                                           ,
			'line_total_amt'          => $this -> decimal(10,2)                -> notNull()                                           ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
		$this->addForeignKey('fk_invoice_item_invoice_id', '{{%invoice_item}}' , 'invoice_id', '{{%invoice}}', 'id' );
		$this->addForeignKey('fk_invoice_item_order_item_id', '{{%invoice_item}}' , 'order_item_id', '{{%order_item}}', 'id' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('fk_invoice_item_invoice_id', '{{%invoice_item}}');
		$this->dropForeignKey('fk_invoice_item_order_item_id', '{{%invoice_item}}');
		$this->dropTable('{{%invoice_item}}');
    }
}
