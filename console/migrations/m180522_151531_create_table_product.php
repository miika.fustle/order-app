<?php

use yii\db\Migration;

/**
 * Class m180522_151531_create_table_product
 */
class m180522_151531_create_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
		$this->createTable('{{%product}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'name'                    => $this -> string(255)                  -> notNull()                                           ,
			'category'                => $this -> string(255)                  -> notNull()                                           ,
			'country'                 => $this -> string(2)                    -> notNull()                                           ,
			'price'                   => $this -> decimal(10,2)                -> notNull()                                           ,
			'tax_percent'             => $this -> decimal(5,4)                 -> notNull()                                           ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
		//$this->addForeignKey('fk_product_category', '{{%product}}', 'category_id', '{{%category}}', 'id');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown() {
		//$this->dropForeignKey('fk_product_category'        , '{{%product}}');
		$this->dropTable('{{%product}}');
    }
}
