<?php

use yii\db\Migration;

/**
 * Class m180523_135413_create_table_invoice
 */
class m180523_135413_create_table_invoice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%invoice}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'order_id'                => $this -> integer(10)    -> unsigned() -> notNull()                                           ,
			'customer_email'          => $this -> string(255)                  -> notNull()                                           ,
			'send_mode'               => "ENUM ('EMAIL') NULL DEFAULT NULL"                                                           ,
			'format'                  => "ENUM ('JSON', 'HTML','PDF') NOT NULL DEFAULT 'JSON'"                                        ,
			'country'                 => $this -> string(255)                  -> notNull()                                           ,
			'sub_total_amt'           => $this -> decimal(10,2)                -> notNull()                                           ,
			'tax_amt'                 => $this -> decimal(10,2)                -> notNull()                                           ,
			'total_amt'               => $this -> decimal(10,2)                -> notNull()                                           ,
			'status'                  => "ENUM ('SENT', 'PROCESSING', 'COMPLETED') NOT NULL DEFAULT 'SENT'"                           ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
		$this->addForeignKey('fk_invoice_order_id', '{{%invoice}}' , 'order_id', '{{%order}}', 'id' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('fk_invoice_order_id', '{{%invoice}}');		
		$this->dropTable('{{%invoice}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180523_135413_create_table_invoice cannot be reverted.\n";

        return false;
    }
    */
}
