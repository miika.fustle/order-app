<?php

use yii\db\Migration;

/**
 * Class m180522_151458_create_table_order
 */
class m180522_151458_create_table_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
		$this->createTable('{{%order}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'customer_email'          => $this -> string(255)                  -> notNull()                                           ,
			'send_mode'               => "ENUM ('EMAIL') NULL DEFAULT NULL"                                                           ,
			'format'                  => "ENUM ('JSON', 'HTML','PDF') NOT NULL DEFAULT 'JSON'"                                        ,
			'country'                 => $this -> string(255)                  -> notNull()                                           ,
			'sub_total_amt'           => $this -> decimal(10,2)                -> notNull()                                           ,
			'tax_amt'                 => $this -> decimal(10,2)                -> notNull()                                           ,
			'total_amt'               => $this -> decimal(10,2)                -> notNull()                                           ,
			'status'                  => "ENUM ('SUBMITTED', 'COMPLETED') NOT NULL DEFAULT 'SUBMITTED'"                               ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,                                          
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
		$this->dropTable('{{%order}}');
    }
}
