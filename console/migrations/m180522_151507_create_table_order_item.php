<?php

use yii\db\Migration;

/**
 * Class m180522_151507_create_table_order_item
 */
class m180522_151507_create_table_order_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
		$this->createTable('{{%order_item}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'order_id'                => $this -> integer(10)    -> unsigned() -> notNull()                                           ,
			'name'                    => $this -> text()                       -> notNull()                                           ,
			'qty'                     => $this -> integer(10)                  -> notNull()                                           ,
			'line_amt'    			  => $this -> decimal(10,2)                -> notNull()                                           ,
			'line_tax'                => $this -> decimal(10,2)                -> notNull()                                           ,
			'line_tax_amt'            => $this -> decimal(10,2)                -> notNull()                                           ,
			'line_total_amt'          => $this -> decimal(10,2)                -> notNull()                                           ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
		$this->addForeignKey('fk_order_item_order_id', '{{%order_item}}', 'order_id', '{{%order}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
		$this->dropForeignKey('fk_order_item_order_id', '{{%order_item}}');
		$this->dropTable('{{%order_item}}');
    }


}
