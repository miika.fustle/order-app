<?php

use yii\db\Migration;

/**
 * Class m180523_072525_insert_table_category
 */
class m180523_072525_insert_table_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$sql =
		"INSERT INTO t_category (category ,created_by, updated_by) VALUES
			('GROCERY', 1, 1),
			('CLOTHES', 1, 1),
			('ELECTRONICS', 1, 1)
		";
		$this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$sql =
		"DELETE FROM t_category WHERE category IN (
			'GROCERY'                ,
			'CLOTHES'                ,
			'ELECTRONICS'              
		)";
		$this->execute($sql);
    }
}
