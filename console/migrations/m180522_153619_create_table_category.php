<?php

use yii\db\Migration;

/**
 * Class m180522_153619_create_table_category
 */
class m180522_153619_create_table_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
		$this->createTable('{{%category}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'category'                => $this -> string(255)                  -> notNull()                                           ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
		$this->dropTable('{{%category}}');
    }
}
