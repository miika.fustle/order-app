<?php

use yii\db\Migration;

/**
 * Class m180526_051042_create_table_custom_calculation
 */
class m180526_051042_create_table_custom_calculation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%custom_calculation}}', [
			'id'                      => $this -> primaryKey(10) -> unsigned()                                                        ,
			'max_price'               => $this -> decimal(10,2)                -> notNull()                                           ,
			'operation'               => "ENUM ('ADD', 'DIVIDE', 'MULTIPLY', 'SUBSTRACT') NOT NULL DEFAULT 'ADD'"                     ,
			'amount'    			  => $this -> decimal(10,2)                -> notNull()                                           ,
			'description'             => $this -> text()                                                                              ,
			'is_active'               => $this -> boolean()                    -> notNull() -> defaultValue(true)                     ,
			'created_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'created_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP') ,
			'updated_by'              => $this -> integer(10)    -> unsigned() -> notNull() -> defaultValue(1)                        ,
			'updated_at'              => $this -> datetime()                   -> notNull() -> defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP') ,
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%custom_calculation}}');
    }

}
